/* 
 * This file is part of Mine Sweeper Simulator.
 * 
 * Copyright 2013, Carlos Alegría Galicia
 *
 * Mine Sweeper Simulator is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later
 * version.
 *
 * Mine Sweeper Simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mine Sweeper Simulator. If not, see
 * <http://www.gnu.org/licenses/>.
 */
package um.nibble.minesweeper.simulator.core;

/**
 * @author Carlos Alegría Galicia
 *
 */
public interface SimulationListener {

	/**
	 * @param e
	 */
	public void onException(SimulationException e);
	
	/**
	 * @param state
	 */
	public void gameStateChanged(GameEvent event);
	
	/**
	 * @param state
	 */
	public void boardStateChanged(BoardEvent event);
}
