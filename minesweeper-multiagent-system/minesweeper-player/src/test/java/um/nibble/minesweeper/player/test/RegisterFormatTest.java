/* 
 * This file is part of Mine Sweeper Simulator.
 * 
 * Copyright 2013, Carlos Alegría Galicia
 *
 * Mine Sweeper Simulator is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later
 * version.
 *
 * Mine Sweeper Simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mine Sweeper Simulator. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package um.nibble.minesweeper.player.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.testng.annotations.Test;

public class RegisterFormatTest {
	
	/* */
	private final Pattern pattern = Pattern.compile("\\(REG (?:(OK) (P1|P2)|(NO) \"((?:\\w| ){0,30})\")\\)");
	/* */
	private final Matcher matcher = pattern.matcher("");
	
	@Test(description="Correct messages")
	public void test1() {
		matcher.reset("(REG OK P1)");
		assert matcher.lookingAt();
		
		matcher.reset("(REG OK P2)");
		assert matcher.lookingAt();
		
		matcher.reset("(REG NO \"\")");
		assert matcher.lookingAt();
		
		matcher.reset("(REG NO \"jsldote sdfe jl8s 8sj jd72 sl8\")");
		assert matcher.lookingAt();
	}
	
	@Test(description="Incorrect messages")
	public void test2() {
		matcher.reset("(REG OK P3)");
		assert !matcher.lookingAt();
		
		matcher.reset("(REG OK ");
		assert !matcher.lookingAt();
		
		matcher.reset("(REG NO )");
		assert !matcher.lookingAt();
		
		matcher.reset("(REG NO \"jsldote sdfe jl8s 8sj jd72 sl8sdfsdfsdfsdfsfd\")");
		assert !matcher.lookingAt();
	}
	
	@Test(description="Extracting groups")
	public void test3() {
		matcher.reset("(REG OK P1)");
		assert matcher.lookingAt();
		
		System.out.println("Group 1: " + matcher.group(1));
		System.out.println("Group 2: " + matcher.group(2));
		
		matcher.reset("(REG NO \"panchito gomez\")");
		assert matcher.lookingAt();
		
		System.out.println("Group 3: " + matcher.group(3));
		System.out.println("Group 4: " + matcher.group(4));
	}
}
