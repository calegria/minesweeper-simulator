/* 
 * This file is part of Mine Sweeper Simulator.
 * 
 * Copyright 2013, Carlos Alegría Galicia
 *
 * Mine Sweeper Simulator is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later
 * version.
 *
 * Mine Sweeper Simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mine Sweeper Simulator. If not, see
 * <http://www.gnu.org/licenses/>.
 */

package um.nibble.minesweeper.player.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.testng.annotations.Test;

public class GameStateFormatTest {
	
	/* */
	private final Pattern pattern = Pattern.compile("\\((GS) (\\d|[1-9]\\d*) (ON|TIE|WP1|WP2)\\)");
	/* */
	private final Matcher matcher = pattern.matcher("");
	
	@Test(description="Correct messages")
	public void test1() {
		matcher.reset("(GS 0 ON)");
		assert matcher.lookingAt();
		
		matcher.reset("(GS 1231 TIE)");
		assert matcher.lookingAt();
		
		matcher.reset("(GS 0 WP1)");
		assert matcher.lookingAt();
		
		matcher.reset("(GS 4353 WP2)");
		assert matcher.lookingAt();
	}
	
	@Test(description="Incorrect messages")
	public void test2() {
		matcher.reset("(GS 123 P)");
		assert !matcher.lookingAt();
		
		matcher.reset("(GS 123 OK ");
		assert !matcher.lookingAt();
		
		matcher.reset("(REG 12 WP1 )");
		assert !matcher.lookingAt();
		
		matcher.reset("(GS 0 ON)dasdas");
		assert matcher.lookingAt();
	}
	
	@Test(description="Extracting groups")
	public void test3() {
		matcher.reset("(GS 0 ON)");
		assert matcher.lookingAt();
		
		System.out.println("Group 1: " + matcher.group(1));
		System.out.println("Group 2: " + matcher.group(2));
		System.out.println("Group 2: " + matcher.group(3));
	}
}
